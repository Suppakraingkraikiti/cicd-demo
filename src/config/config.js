var Etcd = require('node-etcd');
var etcd_endpoint = process.env.etcd_endpoint || 'etcd.snail.arctic.true.th/core';
var etcd = new Etcd(etcd_endpoint);
var config = {};

var appconf = process.env.appconf || "nodejs-demo";
var env = process.env.env || "dev";
var watchTime = process.env.etcd_watch_rate || 5000;

if (watchTime != 0) {
  setInterval(() => {
    changeConfig();
  }, watchTime);
}

function changeConfig() {
  var intset = etcd.getSync(`app/${appconf}/${env}/intset`).body.node.value;
  etcd.get('integration/int/' + intset, { recursive: true }, (err, value) => {
    if (err) {
      console.log(err);
    } else {
      value.node.nodes.forEach((val) => {
        let name = val.key.substr(val.key.lastIndexOf('/') + 1);
        config[name] = val.value;
      });
    }
  });
  etcd.get(`app/${appconf}/${env}`, { recursive: true }, (err, value) => {
    if (err) {
      console.log(err);
    } else {
      value.node.nodes.forEach((val) => {
        let name = val.key.substr(val.key.lastIndexOf('/') + 1);
        config[name] = val.value;
      });
    }
  });
}

changeConfig();

module.exports = config;